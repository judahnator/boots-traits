Boots Traits Library
====================

[![pipeline status](https://gitlab.com/judahnator/boots-traits/badges/master/pipeline.svg)](https://gitlab.com/judahnator/boots-traits/commits/master)

This is a super straightforward library heavily inspired by the Laravel framework. This library allows you to "boot" and "initialize" traits on any given class in an understandable and predictable way.

Installation
============

You can install this library just like you would anything else via composer.

`composer require judahnator/boots-traits`

Usage
=====

Booting
-------

In this context, 'booting' a trait involves calling a static method in it. This is commonly used for registering functionality or setting the class up for future use.

In order to use this functionality you need to 'use' the `\judahnator\BootsTraits\BootsTraits` trait in your class and call its static `bootTraits()` method.

To write a trait that is 'booted' simply write a static method that is named 'boot' then your trait basename. So if your trait was named 'MyAwesomeTrait' your boot method would be named 'bootMyAwesomeTrait' 

Initialization
--------------

'Initializing' a trait is calling a special non-static method on your given trait. This is where you can perform setup actions that require the class to be already booted and have all the services registered available.

In order to use this functionality you need to 'use' the `\judahnator\BootsTraits\InitializesTraits` trait in your class and call its `initializeTraits()` method.

To write a trait that is 'initialized' simply write a method that is named 'initialize' then your trait basename. If your trait was named 'MyAwesomeTrait' your initialization method would be named 'initializeMyAwesomeTrait'.

Example
=======

```php
<?php

use judahnator\BootsTraits\{BootsTraits, InitializesTraits};

// Example booted trait
trait MyBootedTrait
{
    protected static function bootMyBootedTrait(): void
    {
        echo "Booting ".__TRAIT__.PHP_EOL;
    }
}

// Example initialized trait
trait MyInitializedTrait
{
    protected function initializeMyInitializedTrait(): void
    {
        echo __TRAIT__." has been initialized".PHP_EOL;
    }
}

// Why not both initialize and boot a trait?
trait MyBootedAndInitializedTrait
{
    protected static function bootMyBootedAndInitializedTrait(): void
    {
        echo "Booting ".__TRAIT__.PHP_EOL;
    }

    protected function initializeMyBootedAndInitializedTrait(): void
    {
        echo __TRAIT__." has been initialized".PHP_EOL;
    }
}

// To create a class that boots
class MyClassThatBoots
{
    use 
        BootsTraits, // use this to enable booting
        MyBootedTrait; // add your 'bootable' traits
        
    public function __construct() 
    {
        // You can call this from wherever
        // I just find the constructor handy
        static::bootTraits();
    }
}

// To create a class that initializes
class MyClassThatInitializes
{
    use 
        InitializesTraits, // use this to enable initialization
        MyInitializedTrait; // add your 'initializable' traits
}

// You can call the 'initializeTraits()' method from anywhere
(new MyClassThatInitializes())->initializeTraits();

// An example of a class that does both
class MyClassThatBootsAndInitializes
{
    use
        BootsTraits,
        InitializesTraits,
        MyBootedTrait,
        MyInitializedTrait,
        MyBootedAndInitializedTrait;
    
    public function __construct() 
    {
        // Boot your class
        static::bootTraits();
        // Booting MyBootedTrait
        // Booting MyBootedAndInitializedTrait
        
        // Initialize your class
        $this->initializeTraits();
        // MyInitializedTrait has been initialized
        // MyBootedAndInitializedTrait has been initialized
    }
}
```