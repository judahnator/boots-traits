<?php

namespace judahnator\BootsTraits;

use judahnator\TraitAware\TraitAware;

trait BootsTraits
{
    use TraitAware;

    final public static function bootTraits(): void
    {
        $static = static::class;
        foreach (static::getTraits() as $trait) {
            try {
                $baseName = (new \ReflectionClass($trait))->getShortName();
            } catch (\ReflectionException $e) {
                continue;
            }
            if (method_exists($static, $method = "boot{$baseName}")) {
                $static::{$method}();
            }
        }
    }
}
