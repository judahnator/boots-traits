<?php

namespace judahnator\BootsTraits;

use judahnator\TraitAware\TraitAware;

trait InitializesTraits
{
    use TraitAware;

    final public function initializeTraits(): void
    {
        foreach (static::getTraits() as $trait) {
            try {
                $baseName = (new \ReflectionClass($trait))->getShortName();
            } catch (\ReflectionException $e) {
                continue;
            }
            if (method_exists($this, $method = "initialize{$baseName}")) {
                $this->{$method}();
            }
        }
    }
}
