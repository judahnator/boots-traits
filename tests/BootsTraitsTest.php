<?php

namespace judahnator\BootsTraits\Tests;

use judahnator\BootsTraits\BootsTraits;
use PHPUnit\Framework\TestCase;

final class BootsTraitsTest extends TestCase
{
    public function testBootsTraitsTrait(): void
    {
        $class = new TestBootingTraits();
        $this->assertTrue(in_array('bootTestFooTraitBooting', $class::$booted));
        $this->assertTrue(in_array('bootTestBarTraitBooting', $class::$booted));
    }
}

trait TestFooTraitBooting
{
    public static function bootTestFooTraitBooting(): void
    {
        static::$booted[] = __FUNCTION__;
    }
}

trait TestBarTraitBooting
{
    public static function bootTestBarTraitBooting(): void
    {
        static::$booted[] = __FUNCTION__;
    }
}

final class TestBootingTraits
{
    public static $booted = [];

    use BootsTraits, TestFooTraitBooting, TestBarTraitBooting;

    public function __construct()
    {
        static::bootTraits();
    }
}
