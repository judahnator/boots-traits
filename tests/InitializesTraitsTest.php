<?php

namespace judahnator\BootsTraits\Tests;

use PHPUnit\Framework\TestCase;

final class InitializesTraitsTest extends TestCase
{
    public function testTraitInitialization(): void
    {
        $class = new TestInitializesTraits();
        $class->initializeTraits();
        $this->assertTrue(in_array('initializeTestFooInitializationTrait', $class->initialized));
        $this->assertTrue(in_array('initializeTestBarInitializationTrait', $class->initialized));
    }
}

trait TestFooInitializationTrait
{
    public function initializeTestFooInitializationTrait(): void
    {
        $this->initialized[] = __FUNCTION__;
    }
}

trait TestBarInitializationTrait
{
    public function initializeTestBarInitializationTrait(): void
    {
        $this->initialized[] = __FUNCTION__;
    }
}

class TestInitializesTraits
{
    use \judahnator\BootsTraits\InitializesTraits, TestFooInitializationTrait, TestBarInitializationTrait;

    public $initialized = [];
}
